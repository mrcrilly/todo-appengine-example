# App Engine Microservice Example

This repository serves as an example microservice using Google's App Engine
Standard Evironment.

**It's by no means production ready code and is strictly for educational
pruposes only.**

## Deploy

Make sure you have a Google Compute Platform account setup, you have the
`gcloud` suite of CLI tools installed, including the Go extensions, and you've
authenticated against GCP.

Deployment is a simple `gcloud app deploy`, and taking it down is a matter
`gcloud app services delete todo`.

## Insomnia
The `insomnia/` directory contains a JSON file which can be imported into
[Insomnia](https://insomnia.rest/) for testing the IP once it's been deployed, either locally or to App Engine.

## Licence
MIT.
