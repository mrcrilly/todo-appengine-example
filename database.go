package main

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"google.golang.org/appengine/datastore"
)

type todoItem struct {
	ID    string `json:"id"`
	Thing string `json:"thing"`
	Done  bool   `json:"done"`
}

func newUniqueID() string {
	u, _ := uuid.NewV4()
	return u.String()
}

func dbAddItem(c context.Context, i *todoItem) (string, error) {
	i.ID = newUniqueID()
	key := datastore.NewKey(c, "todoItem", i.ID, 0, nil)
	_, err := datastore.Put(c, key, i)
	return i.ID, err
}

func dbDeleteItem(c context.Context, id string) error {
	key := datastore.NewKey(c, "todoItem", id, 0, nil)
	err := datastore.Delete(c, key)
	return err
}

func dbMarkItemComplete(c context.Context, i *todoItem) error {
	i.Done = true
	key := datastore.NewKey(c, "todoItem", i.ID, 0, nil)
	_, err := datastore.Put(c, key, i)
	return err
}

func dbGetItem(c context.Context, id string) (*todoItem, error) {
	key := datastore.NewKey(c, "todoItem", id, 0, nil)
	var userItem todoItem
	err := datastore.Get(c, key, &userItem)
	return &userItem, err
}

func dbGetAllItems(c context.Context) ([]todoItem, error) {
	var todos []todoItem

	_, err := datastore.NewQuery("todoItem").GetAll(c, &todos)
	//panic(keys)
	if err != nil {
		return nil, err
	}

	return todos, nil
}
