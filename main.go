package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"google.golang.org/appengine"
)

type jsonResponse struct {
	Message string
	Data    interface{}
}

func listAll(c *gin.Context) {
	allItems, err := dbGetAllItems(appengine.NewContext(c.Request))
	if err != nil {
		c.JSON(http.StatusInternalServerError, jsonResponse{"error from db", nil})
		return
	}

	c.JSON(http.StatusOK, jsonResponse{"OK", allItems})
}

func deleteItem(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, jsonResponse{"bad ID", nil})
		return
	}

	err := dbDeleteItem(appengine.NewContext(c.Request), id)
	if err != nil {
		c.JSON(http.StatusBadRequest, jsonResponse{"bad ID", nil})
		return
	}

	c.JSON(http.StatusOK, jsonResponse{"OK", nil})
}

func getItem(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, jsonResponse{"bad ID", nil})
		return
	}

	item, err := dbGetItem(appengine.NewContext(c.Request), id)
	if err != nil {
		c.JSON(http.StatusNotFound, jsonResponse{"not found", nil})
		return
	}

	c.JSON(http.StatusOK, jsonResponse{"OK", item})
}

func newItem(c *gin.Context) {
	var newItem todoItem
	err := c.ShouldBindJSON(&newItem)
	if err != nil {
		c.JSON(http.StatusInternalServerError, jsonResponse{"bad JSON", nil})
		return
	}

	id, err := dbAddItem(appengine.NewContext(c.Request), &newItem)
	if err != nil {
		c.JSON(http.StatusInternalServerError, jsonResponse{"error from db", nil})
		return
	}

	c.JSON(http.StatusOK, jsonResponse{"OK", id})
}

func completeItem(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, jsonResponse{"bad ID", nil})
		return
	}

	ctx := appengine.NewContext(c.Request)
	item, err := dbGetItem(ctx, id)
	if err != nil {
		c.JSON(http.StatusNotFound, jsonResponse{"not found", nil})
		return
	}

	err = dbMarkItemComplete(ctx, item)
	if err != nil {
		c.JSON(http.StatusBadRequest, jsonResponse{"error from db", nil})
		return
	}

	c.JSON(http.StatusOK, jsonResponse{"OK", nil})
}

func init() {
	router := gin.New()
	router.GET("/", listAll)
	router.POST("/new", newItem)
	router.GET("/item", getItem)
	router.DELETE("/item", deleteItem)
	router.GET("/item/complete", completeItem)

	http.Handle("/", router)
}

func main() {
	appengine.Main()
}
